
INTRODUCTION
------------
This is a simple add-on for the context module which includes a new context
reaction option called "Hide a Field(s)". If selected it will proceed to hide
a selected node field (or multiple fields) when viewing a full node.

* For a full description of the module, visit the project page:
  https://www.drupal.org/sandbox/plinthify/2688581


REQUIREMENTS
------------

This module requires the following modules:

* Context (https://drupal.org/project/context)


INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. See:
  https://drupal.org/documentation/install/modules-themes/modules-7
  for further information.


CONFIGURATION
-------------

* The module has no menu or modifiable settings. There is no configuration.
  When enabled, there will be a new context reaction option "hide a field(s)".


MAINTAINERS
-----------

Current maintainers:
* Rob Parker (robdubparker) - http://drupal.org/user/1038128

This project has been sponsored by:
* Entercom Communications (http://www.entercom.com/).
* Plinthify Inc.
