<?php
/**
 * @file
 * Contains the overridden context reaction plugin.
 */

/**
 * Class ContextHideFieldReaction. Hides node field(s) when conditions are met.
 */
class ContextHideFieldReaction extends context_reaction {

  /**
   * Options form.
   */
  public function options_form($context) {

    $form = array();
    $form['fields'] = array(
      '#type' => 'fieldset',
      '#title' => "Select fields to hide",
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );
    $form['fields']['hide'] = array(
      '#type' => 'checkboxes',
      '#title' => t("Hide the following fields for the current node."),
      '#options' => $this->getFieldsHideOptions(),
      '#default_value' => $this->getFieldsHideDefaultValue($context),
    );

    return $form;
  }

  /**
   * Provides a default for hide field options.
   */
  private function getFieldsHideDefaultValue($context) {
    $values = $this->fetch_from_context($context);
    return isset($values['fields']) ? $values['fields']['hide'] : array();
  }

  /**
   * Retrieve hide field options.
   */
  private function getFieldsHideOptions() {
    $fields = array();
    foreach (field_info_instances('node') as $v) {
      foreach ($v as $key => $value) {
        $fields[$key] = $value['label'];
      }
    }
    asort($fields);
    return $fields;
  }

  /**
   * Executes the form.
   */
  public function execute(&$build) {
    foreach ($this->get_contexts() as $context) {
      if (isset($context->reactions[$this->plugin]['fields'])) {
        $this->executeByFields(
          $context->reactions[$this->plugin]['fields']['hide'],
          $build
        );
      }
    }
  }

  public function nodeFormExecute(&$form) {
    $y = $this->get_contexts();
    foreach ($this->get_contexts() as $context) {
      if (isset($context->reactions[$this->plugin]['fields'])) {
        $this->executeByFormFields(
          $context->reactions[$this->plugin]['fields']['hide'],
          $form
        );
      }
    }
  }

  /**
   * Sets the field access to false if necessary.
   */
  private function executeByFields($fields, &$build) {
    foreach (array_filter($fields) as $name => $label) {
      if (isset($build[$name])) {
        $build[$name]['#access'] = FALSE;
      }
    }
  }

  /**
   * Sets the field access to false if necessary.
   */
  private function executeByFormFields($fields, &$form) {
    foreach (array_filter($fields) as $name => $label) {
      if (isset($form[$name])) {
        $form[$name]['#access'] = FALSE;
      }
    }
  }

}
